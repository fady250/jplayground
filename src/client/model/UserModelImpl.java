package client.model;

import client.networking.IClient;
import javafx.beans.property.SimpleStringProperty;
import misc.Status;

public class UserModelImpl implements IUserModel {

    private final IClient client;
    private final SimpleStringProperty userName = new SimpleStringProperty() ;

    public UserModelImpl(IClient client) {
        this.client = client;
        userName.bindBidirectional(this.client.userNameProperty());
    }

    @Override
    public Status registerUser(String username, String password) {
        // calls the RMI interface method to send the user info to server , then server will add it to database
        return client.registerUser(username,password);
    }

    @Override
    public Status loginUser(String username, String password) {
        this.userName.setValue(username);    // set the username in the model, will be reflected in the client (server can ask it)
        return client.loginUser(username,password);
    }

    @Override
    public void logoutUser(String username) {
        client.logoutUser(username);
    }
}
