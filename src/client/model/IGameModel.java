package client.model;

import javafx.collections.ObservableList;
import misc.GameState;
import misc.ScoreEntry;

import java.beans.PropertyChangeListener;

public interface IGameModel {
    void addListener(String eventName, PropertyChangeListener listener);
    void removeListeners();
    void insertPlayerIntoGameQueue(String game);
    void updateGameState(String game,GameState state);
    void gameWindowClosed(String game);
    ObservableList<ScoreEntry> getScores(String game);
}
