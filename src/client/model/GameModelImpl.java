package client.model;

import client.networking.IClient;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import misc.*;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

public class GameModelImpl implements IGameModel{

    private final IClient client;
    private PropertyChangeSupport changeSupport = null;
    private char role;

    public GameModelImpl(IClient client) {
        this.client = client;
        client.addListener("RoleUpdate",this::updateRole);
        client.addListener("GuiUpdate",this::updateGui);
        client.addListener("GrantTurn",this::grantTurn);
        client.addListener("UpdateUserMsg",this::updateUserMsg);
    }

    private void updateUserMsg(PropertyChangeEvent evt) {
        // let the model decide what string should be shown
        if(changeSupport != null) {
            if (evt.getNewValue().equals(Notifications.OPPONENT_QUIT)) {
                changeSupport.firePropertyChange("UpdateUserMsg", null, "Opponent quit - you technically WON !");
            } else if (evt.getNewValue().equals(Notifications.YOU_LOST)) {
                changeSupport.firePropertyChange("UpdateUserMsg", null, "You Lost !");
            } else if (evt.getNewValue().equals(Notifications.GAME_TIE)) {
                changeSupport.firePropertyChange("UpdateUserMsg", null, "Its a Tie !");
            }
        }
    }

    private void grantTurn(PropertyChangeEvent evt) {
        changeSupport.firePropertyChange("GrantTurn",null,null);
    }

    private void updateGui(PropertyChangeEvent evt) {
        changeSupport.firePropertyChange("GuiUpdate",null,evt.getNewValue());
    }

    private void updateRole(PropertyChangeEvent evt) {
        role = (char)evt.getNewValue();
        changeSupport.firePropertyChange("RoleUpdate",null,evt.getNewValue());
    }

    /*
     * Add listener to the model property - according to event name
     */
    @Override
    public void addListener(String eventName, PropertyChangeListener listener) {
        if(changeSupport == null){
            changeSupport = new PropertyChangeSupport(this);
        }
        changeSupport.addPropertyChangeListener(eventName,listener);
    }

    @Override
    public void removeListeners() {
        changeSupport = null;
    }

    @Override
    public void insertPlayerIntoGameQueue(String game) {
        client.insertPlayerIntoGameQueue(game);
    }

    @Override
    public void updateGameState(String game,GameState state) {
        if(checkIfPlayerWon(game,state)){
            state.setAsWinner();
            changeSupport.firePropertyChange("UpdateUserMsg",null,"You Won !");
        } else if(checkIfGameTie(game,state)){
            state.setGameTie();
            changeSupport.firePropertyChange("UpdateUserMsg",null,"its a Tie !");
        }
        client.updateGameState(state);
    }

    @Override
    public void gameWindowClosed(String game) {
        client.gameWindowClosed(game);
    }

    @Override
    public ObservableList<ScoreEntry> getScores(String game) {
        ArrayList<String> list = client.getScores(game).getEntries();
        ObservableList<ScoreEntry> scores = FXCollections.observableArrayList();
        for(int i=0;i<list.size();i+=2){
            scores.add(new ScoreEntry(list.get(i), list.get(i+1)));
        }
        return scores;
    }


    private boolean checkIfPlayerWon(String game,GameState state) {
        char[][] board = null;
        if(game.equals("xo")){
            board = new char[3][3];
            board = ((GameStateXo)state).getBoard();
            //Row 1
            assert board != null;
            if(!" ".equals(board[0][0] + "") && (board[0][0] == board[0][1]) && (board[0][1] == board[0][2])){
                return true; //exit from the game
            }
            //Row 2
            if(!" ".equals(board[1][0] + "") && (board[1][0] == board[1][1]) && (board[1][1] == board[1][2])){
                return true;
            }
            //Row 3
            if(!" ".equals(board[2][0] + "") && (board[2][0] == board[2][1]) && (board[2][1] == board[2][2])){
                return true;
            }
            //Column 1
            if(!" ".equals(board[0][0] + "") && (board[0][0] == board[1][0]) && (board[1][0] == board[2][0])){
                return true;
            }
            //Column 2
            if(!" ".equals(board[0][1] + "") && (board[0][1] == board[1][1]) && (board[1][1] == board[2][1])){
                return true;
            }
            //Column 3
            if(!" ".equals(board[0][2] + "") && (board[0][2] == board[1][2]) && (board[1][2] == board[2][2])){
                return true;
            }
            //Diagonal 1
            if(!" ".equals(board[0][0] + "") && (board[0][0] == board[1][1]) && (board[1][1] == board[2][2])){
                return true;
            }
            //Diagonal 2
            if(!" ".equals(board[0][2] + "") && (board[0][2] == board[1][1]) && (board[1][1] == board[2][0])) {
                return true;
            }
        } else if (game.equals("connect4")) {
            board = ((GameStateConnect4) state).getBoard();
            int i = ((GameStateConnect4) state).getI();
            int j = ((GameStateConnect4) state).getJ();

            ArrayList<String> row = new ArrayList<String>();
            ArrayList<String> col = new ArrayList<String>();
            ArrayList<String> diag1 = new ArrayList<String>();
            ArrayList<String> diag2 = new ArrayList<String>();

            //row
            int k = i;
            while (k >= 0) {
                if (board[k][j] == board[i][j])
                    row.add(board[k][j] + "");
                else break;
                ;
                k--;
            }
            k = i + 1;
            while (k < 6) {
                if (board[k][j] == board[i][j])
                    row.add(board[k][j] + "");
                else break;
                ;
                k++;
            }

            //col
            k = j;
            while (k >= 0) {
                if (board[i][k] == board[i][j])
                    col.add(board[i][j] + "");
                else break;
                ;
                k--;
            }
            k = j + 1;
            while (k < 7) {
                if (board[i][j] == board[i][k])
                    col.add(board[i][j] + "");
                else break;
                ;
                k++;
            }

            //diag1
            int r=i;
            int c=j;
            while (r>=0&&c>=0){
                if (board[r][c] == board[i][j])
                    diag1.add(board[i][j] + "");
                else break;
                r--;
                c--;
            }
            r=i+1;
            c=j+1;
            while (r<6&&c<7){
                if (board[r][c] == board[i][j])
                    diag1.add(board[i][j] + "");
                else break;
                r++;
                c++;
            }

            //diag2 [r--,c++] [r++,c--]
            r=i;
            c=j;
            while (r>=0&&c<7){
                if (board[r][c] == board[i][j])
                    diag2.add(board[i][j] + "");
                else break;
                r--;
                c++;
            }
            r=i+1;
            c=j-1;
            while (r<6&&c>=0){
                if (board[r][c] == board[i][j])
                    diag2.add(board[i][j] + "");
                else break;
                r++;
                c--;
            }
            //System.out.println();
            //System.out.print("raw :"+row.size());///////////////////////
            //System.out.print("  col :"+col.size());///////////////////////
            //System.out.print("  diag1 :"+diag1.size());///////////////////////
            //System.out.print("  diag2 :"+diag2.size());///////////////////////
            return row.size() == 4 || col.size() == 4 || diag1.size() == 4 || diag2.size() == 4;
        }
        return false;
    }

    private boolean checkIfGameTie(String game, GameState state) {
        // first check if some1 already won , in this case no need to check for tie
        if (state.getWinningFlag()) {
            return false;
        }

        char[][] board = null;
        if (game.equals("xo")) {
            board = new char[3][3];
            board = ((GameStateXo) state).getBoard();
        } else if (game.equals("connect4")) {
            board = new char[6][7];
            board = ((GameStateConnect4) state).getBoard();

        }

        for (char[] chars : board) {
            for (char c : chars) {
                if (c == ' ')
                    return false;
            }
        }
        return true;
    }
}
