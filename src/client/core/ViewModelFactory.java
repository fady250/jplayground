package client.core;


import client.view.connect4.Connect4BoardViewModel;
import client.view.main.MainWindowViewModel;
import client.view.menu.GamesMenuViewModel;
import client.view.register.RegisterViewModel;
import client.view.score.ScoreWindowViewModel;
import client.view.xo.XoBoardViewModel;

public class ViewModelFactory {
    private final ModelFactory mf;
    private MainWindowViewModel MainWindowVM;
    private RegisterViewModel RegisterVM;
    private GamesMenuViewModel GamesMenuVM;
    private XoBoardViewModel XoBoardVM;
    private Connect4BoardViewModel Connect4VM;
    private ScoreWindowViewModel scoreVM;
    // more view models to come here

    public ViewModelFactory(ModelFactory mf){
        this.mf = mf;
    }

    public MainWindowViewModel getMainWindowVM() {
        if (MainWindowVM == null) {
            MainWindowVM = new MainWindowViewModel(mf.getUserModel());      // send specific model
        }
        return MainWindowVM;
    }

    public RegisterViewModel getRegisterVM(){
        if (RegisterVM == null) {
            RegisterVM = new RegisterViewModel(mf);                   // send all the model factory
        }
        return RegisterVM;
    }

    public GamesMenuViewModel getGamesMenueVM(){
        if (GamesMenuVM == null) {
            GamesMenuVM = new GamesMenuViewModel(mf);                 // send all the model factory
        }
        return GamesMenuVM;
    }

    public XoBoardViewModel getXoBoardVM(){
        if (XoBoardVM == null) {
            XoBoardVM = new XoBoardViewModel(mf);                     // send all the model factory ( no real reason for that)
        }
        return XoBoardVM;
    }

    public Connect4BoardViewModel getConnect4BoardVM(){
        if (Connect4VM == null) {
            Connect4VM = new Connect4BoardViewModel(mf.getGameModel());     // send only game model
        }
        return Connect4VM;
    }

    public ScoreWindowViewModel getScoreVM(){
        if (scoreVM == null) {
            scoreVM = new ScoreWindowViewModel(mf.getGameModel());     // send only game model
        }
        return scoreVM;
    }
}
