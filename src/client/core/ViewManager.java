package client.core;

import client.view.connect4.Connect4BoardController;
import client.view.main.MainWindowController;
import client.view.menu.GamesMenuController;
import client.view.register.RegisterController;
import client.view.score.ScoreWindowController;
import client.view.xo.XoBoardController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;


public class ViewManager {

    private final Stage mainStage;                            // main window will change to games menu after login
    private Parent root = null;
    private final ViewModelFactory vmf;
    private String userName;

    public ViewManager(ViewModelFactory vmf){
        this.mainStage = new Stage();
        this.vmf = vmf;
    }

    public void start() throws Exception {
        changeView("MainWindow");
    }

    public void setUserName(String username){
        this.userName = username;
    }

    public String getUserName() {
        return this.userName;
    }

    public void openRegisterView() {
        Stage registerStage = new Stage();
        Scene sc;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Register.fxml"));
        try {
            root = loader.load();
            RegisterController viewController = loader.getController();
            viewController.init(vmf.getRegisterVM(),this);
            registerStage.setTitle("Register");
            sc = new Scene(root);
            registerStage.setScene(sc);
            registerStage.initModality(Modality.WINDOW_MODAL);
            registerStage.initOwner(mainStage);
            registerStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeView(String view) {
        Scene sc;
        FXMLLoader loader = new FXMLLoader();
        try {
            if ("MainWindow".equals(view)) {
                loader.setLocation(getClass().getResource("MainWindow.fxml"));
                root = loader.load();
                MainWindowController viewController = loader.getController();
                viewController.init(this, vmf.getMainWindowVM());
                mainStage.setTitle("GameCenter");
            } else if ("MenuView".equals(view)) {                  // change the same main stage to the menu
                loader.setLocation(getClass().getResource("GamesMenu.fxml"));
                root = loader.load();
                GamesMenuController viewController = loader.getController();
                viewController.init(this, vmf.getGamesMenueVM());
                mainStage.setTitle("GameCenter");
            }
            sc = new Scene(root);
            mainStage.setScene(sc);
            mainStage.show();
        } catch (IOException e){
            e.printStackTrace();
        }
        mainStage.setOnCloseRequest(ev -> {
            System.exit(0);
        });
    }

    public void openXoView() {
        Stage xoStage = new Stage();
        Scene sc;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("XoBoard.fxml"));
        try{
            root = loader.load();
            XoBoardController viewController = loader.getController();
            viewController.init(this, vmf.getXoBoardVM());
            xoStage.setTitle("X-O");
            sc = new Scene(root);
            xoStage.setScene(sc);
            xoStage.initModality(Modality.WINDOW_MODAL);
            xoStage.initOwner(mainStage);
            xoStage.show();
            vmf.getXoBoardVM().userMessageProperty().setValue("Searching for opponent, please wait");
            vmf.getXoBoardVM().initListeners();
            xoStage.setOnCloseRequest(ev -> {
                viewController.windowClosed();          // inform controller that window was closed
                vmf.getXoBoardVM().removeListeners();
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openConnect4View() {
        Stage connect4 = new Stage();
        Scene sc;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Connect4Board.fxml"));
        try{
            root = loader.load();
            Connect4BoardController viewController = loader.getController();
            viewController.init(vmf.getConnect4BoardVM());
            connect4.setTitle("Connect4");
            sc = new Scene(root);
            connect4.setScene(sc);
            connect4.initModality(Modality.WINDOW_MODAL);
            connect4.initOwner(mainStage);
            connect4.show();
            vmf.getConnect4BoardVM().userMsgProperty().setValue("Searching for opponent, please wait");
            vmf.getConnect4BoardVM().initListeners();
            vmf.getConnect4BoardVM().disableButtons();
            connect4.setOnCloseRequest(ev -> {
                viewController.windowClosed();          // inform controller that window was closed
                vmf.getConnect4BoardVM().removeListeners();
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openScoreView(String game) {
        Stage ScoreStage = new Stage();
        Scene sc;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("Score.fxml"));
        try {
            root = loader.load();
            ScoreWindowController viewController = loader.getController();
            viewController.init(vmf.getScoreVM(),game);                     // send the game to the controller
            ScoreStage.setTitle(game + " Score Table");
            sc = new Scene(root);
            ScoreStage.setScene(sc);
            ScoreStage.initModality(Modality.WINDOW_MODAL);
            ScoreStage.initOwner(mainStage);
            ScoreStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

