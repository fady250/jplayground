package client.core;

import client.networking.IClient;
import client.networking.RMIClient;


public class ClientFactory {
    private IClient client;

    public IClient getClient() {
        if(client == null) {
            //System.setProperty("java.security.policy", "/rmi.policy");
            //    if(System.getSecurityManager() == null){
            //    System.setSecurityManager(new SecurityManager());
            //}
            client = new RMIClient();
            client.startClient();
        }
        return client;
    }
}
