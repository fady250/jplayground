package client.networking;

import javafx.beans.property.StringProperty;
import misc.GameState;
import misc.ScoreEntries;
import misc.Status;

import java.beans.PropertyChangeListener;

public interface IClient {
    void addListener(String eventName, PropertyChangeListener listener);
    StringProperty userNameProperty();
    // methods exposed to the model in the client side
    void startClient();
    Status registerUser(String username, String password);
    Status loginUser(String username, String password);
    void logoutUser(String username);
    void insertPlayerIntoGameQueue(String game);
    void updateGameState(GameState state);
    void gameWindowClosed(String game);
    ScoreEntries getScores(String game);
}
