package client.view.register;

import client.core.ModelFactory;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import misc.Status;

public class RegisterViewModel {

    private ModelFactory mf;

    private StringProperty username = new SimpleStringProperty();
    private StringProperty password = new SimpleStringProperty();
    private StringProperty confirmPassword = new SimpleStringProperty();
    private StringProperty registerResponse = new SimpleStringProperty();
    private StringProperty cancelButtonText = new SimpleStringProperty();
    private BooleanProperty registerButtonDisable = new SimpleBooleanProperty();

    public RegisterViewModel(ModelFactory mf) {
        this.mf = mf;
        cancelButtonText.setValue("Cancel");
    }

    public void registerUser(){
        String user = username.get();
        String pass = password.get();
        if (user == null || user.equals("")) {
            registerResponse.set("Username cannot be empty");
            return;
        }

        if(pass == null || pass.equals("")) {
            registerResponse.set("Password cannot be empty");
            return;
        }

        if (!pass.equals(confirmPassword.get())) {
            registerResponse.set("Passwords do not match");
            return;
        }

        // call the model method which will send it to server using RMI
        Status result = mf.getUserModel().registerUser(user,pass);
        if(result.equals(Status.FAIL_USER_EXIST)){
            registerResponse.set("Username already exists");
            return;
        }

        //error to the user by updating some text field which is bind to the view
        registerResponse.set("Registered Successfully ! You may login now");
        cancelButtonText.setValue("Done");
        registerButtonDisable.setValue(true);
    }

    public void clear() {
        username.set("");
        password.set("");
        confirmPassword.set("");
        registerResponse.set("");
        cancelButtonText.setValue("Cancel");
        registerButtonDisable.setValue(false);
    }

    public StringProperty usernameTextProperty() { return username; }
    public StringProperty passwordTextProperty(){ return password; }
    public StringProperty confirmPasswordTextProperty(){ return confirmPassword; }
    public StringProperty registerResponseTextProperty(){return registerResponse;}
    public StringProperty cancelButtonTextProperty() { return cancelButtonText; }
    public BooleanProperty registerButtonDisableProperty() { return registerButtonDisable; }
}
