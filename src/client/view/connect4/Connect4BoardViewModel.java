package client.view.connect4;

import client.model.IGameModel;
import javafx.application.Platform;
import javafx.beans.property.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import misc.GameStateConnect4;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Connect4BoardViewModel {

    private final IGameModel gameModel;
    private GameStateConnect4 state;
    private char role;                      // color of player
    private String color;

    private final StringProperty userMsg = new SimpleStringProperty();
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);    // to fire change event to controller buttons

    private final BooleanProperty[][] circleVisibility = new BooleanProperty[6][7];
    private final ObjectProperty<Paint>[][] circleColor = new ObjectProperty[6][7];


    public Connect4BoardViewModel(IGameModel gameModel){
        this.gameModel = gameModel;
        this.state = new GameStateConnect4();
        for(int i = 0;i <6;i++) {
            for (int j = 0; j < 7; j++) {
                circleVisibility[i][j] = new SimpleBooleanProperty();
                circleColor[i][j] = new SimpleObjectProperty<>();
            }
        }
    }

    private void updateUserMsg(PropertyChangeEvent evt) {
        userMsg.setValue((String)evt.getNewValue());
        state = new GameStateConnect4();      // restart
        changeSupport.firePropertyChange("enableDisableButtons",null,true); // disable buttons after player clicked
    }

    private void updateRole(PropertyChangeEvent evt) {
        role = (char)evt.getNewValue();
        if(role == 'y'){                    // for first iteration only
            Platform.runLater(() -> userMsg.setValue("You are the : " + (role + "ellow").toUpperCase() + "\n" + " Opponent's Turn Now !"));
            color = "YELLOW";
        }else{
            color = "RED";
        }
    }

    private void grantTurn(PropertyChangeEvent evt) {
        Platform.runLater(() -> userMsg.setValue( "You are the : " + color.toUpperCase() + "\n" + "Your Turn Now !"));
        changeSupport.firePropertyChange("enableDisableButtons",null,false); // enable buttons when turn is granted
    }

    private void updateGui(PropertyChangeEvent evt) {
        state = (GameStateConnect4) evt.getNewValue();
        // update GUI here
        char[][] textBoard = state.getBoard();
        for(int i = 0;i <6;i++) {
            for (int j = 0; j < 7; j++) {
                if(textBoard[i][j] == 'r'){
                    circleVisibility[i][j].set(true);
                    circleColor[i][j].set(Color.RED);
                }else if(textBoard[i][j] == 'y'){
                    circleVisibility[i][j].set(true);
                    circleColor[i][j].set(Color.YELLOW);
                }
            }
        }
    }

    private void buttonHandler(int j){
        for(int i = 5; i>=0 ; i--){
            if(!circleVisibility[i][j].get()){
                if(role == 'r'){
                    circleColor[i][j].set(Color.RED);
                }else {
                    circleColor[i][j].set(Color.YELLOW);
                }
                state.setField(role,i,j);
                circleVisibility[i][j].set(true);
                userMsg.setValue("You are the : " + color.toUpperCase() + "\n" + "Opponent's Turn Now !");
                gameModel.updateGameState("connect4",state);
                break;
            }
        }
        changeSupport.firePropertyChange("enableDisableButtons",null,true); // disable buttons after player clicked
    }

    public void handleb7() {buttonHandler(6);}
    public void handleb6() {buttonHandler(5);}
    public void handleb5() {buttonHandler(4);}
    public void handleb4() {buttonHandler(3);}
    public void handleb3() {buttonHandler(2);}
    public void handleb2() {buttonHandler(1);}
    public void handleb1() {buttonHandler(0);}

    public Property<Boolean> c00boolProperty() {return circleVisibility[0][0];}
    public Property<Boolean> c01boolProperty() {return circleVisibility[0][1];}
    public Property<Boolean> c02boolProperty() {return circleVisibility[0][2];}
    public Property<Boolean> c03boolProperty() {return circleVisibility[0][3];}
    public Property<Boolean> c04boolProperty() {return circleVisibility[0][4];}
    public Property<Boolean> c05boolProperty() {return circleVisibility[0][5];}
    public Property<Boolean> c06boolProperty() {return circleVisibility[0][6];}

    public Property<Boolean> c10boolProperty() {return circleVisibility[1][0];}
    public Property<Boolean> c11boolProperty() {return circleVisibility[1][1];}
    public Property<Boolean> c12boolProperty() {return circleVisibility[1][2];}
    public Property<Boolean> c13boolProperty() {return circleVisibility[1][3];}
    public Property<Boolean> c14boolProperty() {return circleVisibility[1][4];}
    public Property<Boolean> c15boolProperty() {return circleVisibility[1][5];}
    public Property<Boolean> c16boolProperty() {return circleVisibility[1][6];}

    public Property<Boolean> c20boolProperty() {return circleVisibility[2][0];}
    public Property<Boolean> c21boolProperty() {return circleVisibility[2][1];}
    public Property<Boolean> c22boolProperty() {return circleVisibility[2][2];}
    public Property<Boolean> c23boolProperty() {return circleVisibility[2][3];}
    public Property<Boolean> c24boolProperty() {return circleVisibility[2][4];}
    public Property<Boolean> c25boolProperty() {return circleVisibility[2][5];}
    public Property<Boolean> c26boolProperty() {return circleVisibility[2][6];}

    public Property<Boolean> c30boolProperty() {return circleVisibility[3][0];}
    public Property<Boolean> c31boolProperty() {return circleVisibility[3][1];}
    public Property<Boolean> c32boolProperty() {return circleVisibility[3][2];}
    public Property<Boolean> c33boolProperty() {return circleVisibility[3][3];}
    public Property<Boolean> c34boolProperty() {return circleVisibility[3][4];}
    public Property<Boolean> c35boolProperty() {return circleVisibility[3][5];}
    public Property<Boolean> c36boolProperty() {return circleVisibility[3][6];}

    public Property<Boolean> c40boolProperty() {return circleVisibility[4][0];}
    public Property<Boolean> c41boolProperty() {return circleVisibility[4][1];}
    public Property<Boolean> c42boolProperty() {return circleVisibility[4][2];}
    public Property<Boolean> c43boolProperty() {return circleVisibility[4][3];}
    public Property<Boolean> c44boolProperty() {return circleVisibility[4][4];}
    public Property<Boolean> c45boolProperty() {return circleVisibility[4][5];}
    public Property<Boolean> c46boolProperty() {return circleVisibility[4][6];}

    public Property<Boolean> c50boolProperty() {return circleVisibility[5][0];}
    public Property<Boolean> c51boolProperty() {return circleVisibility[5][1];}
    public Property<Boolean> c52boolProperty() {return circleVisibility[5][2];}
    public Property<Boolean> c53boolProperty() {return circleVisibility[5][3];}
    public Property<Boolean> c54boolProperty() {return circleVisibility[5][4];}
    public Property<Boolean> c55boolProperty() {return circleVisibility[5][5];}
    public Property<Boolean> c56boolProperty() {return circleVisibility[5][6];}

    /////////////////////////////////////////////////////////////////////////////////////////

    public Property<Paint> c00PaintProperty() {return circleColor[0][0];}
    public Property<Paint> c01PaintProperty() {return circleColor[0][1];}
    public Property<Paint> c02PaintProperty() {return circleColor[0][2];}
    public Property<Paint> c03PaintProperty() {return circleColor[0][3];}
    public Property<Paint> c04PaintProperty() {return circleColor[0][4];}
    public Property<Paint> c05PaintProperty() {return circleColor[0][5];}
    public Property<Paint> c06PaintProperty() {return circleColor[0][6];}

    public Property<Paint> c10PaintProperty() {return circleColor[1][0];}
    public Property<Paint> c11PaintProperty() {return circleColor[1][1];}
    public Property<Paint> c12PaintProperty() {return circleColor[1][2];}
    public Property<Paint> c13PaintProperty() {return circleColor[1][3];}
    public Property<Paint> c14PaintProperty() {return circleColor[1][4];}
    public Property<Paint> c15PaintProperty() {return circleColor[1][5];}
    public Property<Paint> c16PaintProperty() {return circleColor[1][6];}

    public Property<Paint> c20PaintProperty() {return circleColor[2][0];}
    public Property<Paint> c21PaintProperty() {return circleColor[2][1];}
    public Property<Paint> c22PaintProperty() {return circleColor[2][2];}
    public Property<Paint> c23PaintProperty() {return circleColor[2][3];}
    public Property<Paint> c24PaintProperty() {return circleColor[2][4];}
    public Property<Paint> c25PaintProperty() {return circleColor[2][5];}
    public Property<Paint> c26PaintProperty() {return circleColor[2][6];}

    public Property<Paint> c30PaintProperty() {return circleColor[3][0];}
    public Property<Paint> c31PaintProperty() {return circleColor[3][1];}
    public Property<Paint> c32PaintProperty() {return circleColor[3][2];}
    public Property<Paint> c33PaintProperty() {return circleColor[3][3];}
    public Property<Paint> c34PaintProperty() {return circleColor[3][4];}
    public Property<Paint> c35PaintProperty() {return circleColor[3][5];}
    public Property<Paint> c36PaintProperty() {return circleColor[3][6];}

    public Property<Paint> c40PaintProperty() {return circleColor[4][0];}
    public Property<Paint> c41PaintProperty() {return circleColor[4][1];}
    public Property<Paint> c42PaintProperty() {return circleColor[4][2];}
    public Property<Paint> c43PaintProperty() {return circleColor[4][3];}
    public Property<Paint> c44PaintProperty() {return circleColor[4][4];}
    public Property<Paint> c45PaintProperty() {return circleColor[4][5];}
    public Property<Paint> c46PaintProperty() {return circleColor[4][6];}

    public Property<Paint> c50PaintProperty() {return circleColor[5][0];}
    public Property<Paint> c51PaintProperty() {return circleColor[5][1];}
    public Property<Paint> c52PaintProperty() {return circleColor[5][2];}
    public Property<Paint> c53PaintProperty() {return circleColor[5][3];}
    public Property<Paint> c54PaintProperty() {return circleColor[5][4];}
    public Property<Paint> c55PaintProperty() {return circleColor[5][5];}
    public Property<Paint> c56PaintProperty() {return circleColor[5][6];}

    public Property<String> userMsgProperty() { return userMsg;}

    public void addListener(String eventName, PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(eventName,listener);
    }

    public void windowClosed() {
        gameModel.gameWindowClosed("connect4");
        for(int i = 0;i <6;i++) {
            for (int j = 0; j < 7; j++) {
                circleVisibility[i][j].set(false);
            }
        }
    }

    public void disableButtons(){       // used by view manager to disable buttons at init
        changeSupport.firePropertyChange("enableDisableButtons",null,true); // disable buttons at starting
    }

    public void removeListeners() {
        gameModel.removeListeners();
    }

    public void initListeners() {
        gameModel.addListener("RoleUpdate",this::updateRole);
        gameModel.addListener("GuiUpdate",this::updateGui);
        gameModel.addListener("GrantTurn",this::grantTurn);
        gameModel.addListener("UpdateUserMsg",this::updateUserMsg);
    }
}
