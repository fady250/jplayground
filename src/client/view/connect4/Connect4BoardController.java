package client.view.connect4;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

public class Connect4BoardController {

    public Label userMsg;
    @FXML Button B1;
    @FXML Button B2;
    @FXML Button B3;
    @FXML Button B4;
    @FXML Button B5;
    @FXML Button B6;
    @FXML Button B7;

    ArrayList<Button> buttons;

    @FXML
    Circle c00,c01,c02,c03,c04,c05,c06,
           c10,c11,c12,c13,c14,c15,c16,
           c20,c21,c22,c23,c24,c25,c26,
           c30,c31,c32,c33,c34,c35,c36,
           c40,c41,c42,c43,c44,c45,c46,
           c50,c51,c52,c53,c54,c55,c56;

    private Connect4BoardViewModel connect4VM;
    public void init(Connect4BoardViewModel connect4VM) {
        this.connect4VM = connect4VM;
        buttons = new ArrayList<>();
        buttons.add(B1);buttons.add(B2);buttons.add(B3);buttons.add(B4);buttons.add(B5);buttons.add(B6);buttons.add(B7);

        connect4VM.addListener("enableDisableButtons",this::enableDisableButtons);

        userMsg.textProperty().bindBidirectional(connect4VM.userMsgProperty());
        c00.visibleProperty().bindBidirectional(connect4VM.c00boolProperty());
        c01.visibleProperty().bindBidirectional(connect4VM.c01boolProperty());
        c02.visibleProperty().bindBidirectional(connect4VM.c02boolProperty());
        c03.visibleProperty().bindBidirectional(connect4VM.c03boolProperty());
        c04.visibleProperty().bindBidirectional(connect4VM.c04boolProperty());
        c05.visibleProperty().bindBidirectional(connect4VM.c05boolProperty());
        c06.visibleProperty().bindBidirectional(connect4VM.c06boolProperty());

        c10.visibleProperty().bindBidirectional(connect4VM.c10boolProperty());
        c11.visibleProperty().bindBidirectional(connect4VM.c11boolProperty());
        c12.visibleProperty().bindBidirectional(connect4VM.c12boolProperty());
        c13.visibleProperty().bindBidirectional(connect4VM.c13boolProperty());
        c14.visibleProperty().bindBidirectional(connect4VM.c14boolProperty());
        c15.visibleProperty().bindBidirectional(connect4VM.c15boolProperty());
        c16.visibleProperty().bindBidirectional(connect4VM.c16boolProperty());

        c20.visibleProperty().bindBidirectional(connect4VM.c20boolProperty());
        c21.visibleProperty().bindBidirectional(connect4VM.c21boolProperty());
        c22.visibleProperty().bindBidirectional(connect4VM.c22boolProperty());
        c23.visibleProperty().bindBidirectional(connect4VM.c23boolProperty());
        c24.visibleProperty().bindBidirectional(connect4VM.c24boolProperty());
        c25.visibleProperty().bindBidirectional(connect4VM.c25boolProperty());
        c26.visibleProperty().bindBidirectional(connect4VM.c26boolProperty());

        c30.visibleProperty().bindBidirectional(connect4VM.c30boolProperty());
        c31.visibleProperty().bindBidirectional(connect4VM.c31boolProperty());
        c32.visibleProperty().bindBidirectional(connect4VM.c32boolProperty());
        c33.visibleProperty().bindBidirectional(connect4VM.c33boolProperty());
        c34.visibleProperty().bindBidirectional(connect4VM.c34boolProperty());
        c35.visibleProperty().bindBidirectional(connect4VM.c35boolProperty());
        c36.visibleProperty().bindBidirectional(connect4VM.c36boolProperty());

        c40.visibleProperty().bindBidirectional(connect4VM.c40boolProperty());
        c41.visibleProperty().bindBidirectional(connect4VM.c41boolProperty());
        c42.visibleProperty().bindBidirectional(connect4VM.c42boolProperty());
        c43.visibleProperty().bindBidirectional(connect4VM.c43boolProperty());
        c44.visibleProperty().bindBidirectional(connect4VM.c44boolProperty());
        c45.visibleProperty().bindBidirectional(connect4VM.c45boolProperty());
        c46.visibleProperty().bindBidirectional(connect4VM.c46boolProperty());

        c50.visibleProperty().bindBidirectional(connect4VM.c50boolProperty());
        c51.visibleProperty().bindBidirectional(connect4VM.c51boolProperty());
        c52.visibleProperty().bindBidirectional(connect4VM.c52boolProperty());
        c53.visibleProperty().bindBidirectional(connect4VM.c53boolProperty());
        c54.visibleProperty().bindBidirectional(connect4VM.c54boolProperty());
        c55.visibleProperty().bindBidirectional(connect4VM.c55boolProperty());
        c56.visibleProperty().bindBidirectional(connect4VM.c56boolProperty());

        ////////////////////////////////////////////////////////////////////////////////

        c00.fillProperty().bindBidirectional(connect4VM.c00PaintProperty());
        c01.fillProperty().bindBidirectional(connect4VM.c01PaintProperty());
        c02.fillProperty().bindBidirectional(connect4VM.c02PaintProperty());
        c03.fillProperty().bindBidirectional(connect4VM.c03PaintProperty());
        c04.fillProperty().bindBidirectional(connect4VM.c04PaintProperty());
        c05.fillProperty().bindBidirectional(connect4VM.c05PaintProperty());
        c06.fillProperty().bindBidirectional(connect4VM.c06PaintProperty());

        c10.fillProperty().bindBidirectional(connect4VM.c10PaintProperty());
        c11.fillProperty().bindBidirectional(connect4VM.c11PaintProperty());
        c12.fillProperty().bindBidirectional(connect4VM.c12PaintProperty());
        c13.fillProperty().bindBidirectional(connect4VM.c13PaintProperty());
        c14.fillProperty().bindBidirectional(connect4VM.c14PaintProperty());
        c15.fillProperty().bindBidirectional(connect4VM.c15PaintProperty());
        c16.fillProperty().bindBidirectional(connect4VM.c16PaintProperty());

        c20.fillProperty().bindBidirectional(connect4VM.c20PaintProperty());
        c21.fillProperty().bindBidirectional(connect4VM.c21PaintProperty());
        c22.fillProperty().bindBidirectional(connect4VM.c22PaintProperty());
        c23.fillProperty().bindBidirectional(connect4VM.c23PaintProperty());
        c24.fillProperty().bindBidirectional(connect4VM.c24PaintProperty());
        c25.fillProperty().bindBidirectional(connect4VM.c25PaintProperty());
        c26.fillProperty().bindBidirectional(connect4VM.c26PaintProperty());

        c30.fillProperty().bindBidirectional(connect4VM.c30PaintProperty());
        c31.fillProperty().bindBidirectional(connect4VM.c31PaintProperty());
        c32.fillProperty().bindBidirectional(connect4VM.c32PaintProperty());
        c33.fillProperty().bindBidirectional(connect4VM.c33PaintProperty());
        c34.fillProperty().bindBidirectional(connect4VM.c34PaintProperty());
        c35.fillProperty().bindBidirectional(connect4VM.c35PaintProperty());
        c36.fillProperty().bindBidirectional(connect4VM.c36PaintProperty());

        c40.fillProperty().bindBidirectional(connect4VM.c40PaintProperty());
        c41.fillProperty().bindBidirectional(connect4VM.c41PaintProperty());
        c42.fillProperty().bindBidirectional(connect4VM.c42PaintProperty());
        c43.fillProperty().bindBidirectional(connect4VM.c43PaintProperty());
        c44.fillProperty().bindBidirectional(connect4VM.c44PaintProperty());
        c45.fillProperty().bindBidirectional(connect4VM.c45PaintProperty());
        c46.fillProperty().bindBidirectional(connect4VM.c46PaintProperty());

        c50.fillProperty().bindBidirectional(connect4VM.c50PaintProperty());
        c51.fillProperty().bindBidirectional(connect4VM.c51PaintProperty());
        c52.fillProperty().bindBidirectional(connect4VM.c52PaintProperty());
        c53.fillProperty().bindBidirectional(connect4VM.c53PaintProperty());
        c54.fillProperty().bindBidirectional(connect4VM.c54PaintProperty());
        c55.fillProperty().bindBidirectional(connect4VM.c55PaintProperty());
        c56.fillProperty().bindBidirectional(connect4VM.c56PaintProperty());
    }

    public void windowClosed() {
        connect4VM.windowClosed();
    }


    public void B7Clicked(ActionEvent actionEvent) {connect4VM.handleb7();}
    public void B6Clicked(ActionEvent actionEvent) {connect4VM.handleb6();}
    public void B5Clicked(ActionEvent actionEvent) {connect4VM.handleb5();}
    public void B4Clicked(ActionEvent actionEvent) {connect4VM.handleb4();}
    public void B3Clicked(ActionEvent actionEvent) {connect4VM.handleb3();}
    public void B2Clicked(ActionEvent actionEvent) {connect4VM.handleb2();}
    public void B1Clicked(ActionEvent actionEvent) {connect4VM.handleb1();}

    private void enableDisableButtons(PropertyChangeEvent evt){
        boolean en1_dis0 = (boolean)evt.getNewValue();
        for (Button b :buttons) {
            b.setDisable(en1_dis0);
        }
    }
}
