package client.view.xo;

import client.core.ModelFactory;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;
import misc.GameStateXo;

import java.beans.PropertyChangeEvent;
import java.util.ArrayList;

public class XoBoardViewModel {
    private final ModelFactory mf;
    private GameStateXo state;          // observer
    private char role;                  // observer , whenever the data in the model changes , the data here is updated
    // String properties
    private final ArrayList<StringProperty> buttonsTextProperty = new ArrayList<>();
    private final BooleanProperty gridDisable = new SimpleBooleanProperty();
    private final StringProperty userMessage = new SimpleStringProperty();

    public XoBoardViewModel(ModelFactory mf) {
        this.mf = mf;
        state = new GameStateXo();

        for(int i = 0;i <9;i++){
            buttonsTextProperty.add(new SimpleStringProperty());
        }
        gridDisable.set(true);
    }

    private void updateUserMsg(PropertyChangeEvent evt) {
        userMessage.setValue((String)evt.getNewValue());
        state = new GameStateXo();      // restart
        gridDisable.set(true);
    }

    private void grantTurn(PropertyChangeEvent evt) {
        Platform.runLater(() -> userMessage.setValue( "You are the : " + (role + "").toUpperCase() + "\n" + "Your Turn Now !"));
        gridDisable.set(false);
    }

    public void handleClick(Button b){
        String buttonLabel = b.getText();
        //check if legal click
        if (" ".equals(buttonLabel)) {
            b.setText(role + "");
            state.setField(role,(int)b.getLayoutY()/150,(int)b.getLayoutX()/150);
            userMessage.setValue("You are the : " + (role + "").toUpperCase() + "\n" + "Opponent's Turn Now !");
            mf.getGameModel().updateGameState("xo",state); // ask the model to update the state ( including if player won and send to server)
            //freeze clicks
            gridDisable.set(true);
        }
    }

    private void updateGui(PropertyChangeEvent evt) {
        state = (GameStateXo) evt.getNewValue();
        // update GUI here
        char[][] textBoard = state.getBoard();
        int index = 0;
        for (char[] chars : textBoard) {
            for(char c : chars){
                StringProperty s = buttonsTextProperty.remove(index);
                s.setValue(c + "");
                buttonsTextProperty.add(index,s);
                index++;
            }
        }
    }

    private void updateRole(PropertyChangeEvent evt) {
        role = (char)evt.getNewValue();
        if(role == 'o'){                    // for first iteration only
            Platform.runLater(() -> userMessage.setValue("You are the : " + (role + "").toUpperCase() + "\n" + " Opponent's Turn Now !"));
        }
    }

    public void windowClosed(){
        mf.getGameModel().gameWindowClosed("xo");
    }

    // getters for the string property so we can bind them in the controller
    public StringProperty b1Property() {
        return buttonsTextProperty.get(0);
    }
    public StringProperty b2Property() {
        return buttonsTextProperty.get(1);
    }
    public StringProperty b3Property() {
        return buttonsTextProperty.get(2);
    }
    public StringProperty b4Property() {
        return buttonsTextProperty.get(3);
    }
    public StringProperty b5Property() {
        return buttonsTextProperty.get(4);
    }
    public StringProperty b6Property() {
        return buttonsTextProperty.get(5);
    }
    public StringProperty b7Property() {
        return buttonsTextProperty.get(6);
    }
    public StringProperty b8Property() {
        return buttonsTextProperty.get(7);
    }
    public StringProperty b9Property() {
        return buttonsTextProperty.get(8);
    }
    public BooleanProperty gridDisableProperty() {
        return gridDisable;
    }
    public StringProperty userMessageProperty() {
        return userMessage;
    }

    public void initListeners() {
        mf.getGameModel().addListener("RoleUpdate",this::updateRole);
        mf.getGameModel().addListener("GuiUpdate",this::updateGui);
        mf.getGameModel().addListener("GrantTurn",this::grantTurn);
        mf.getGameModel().addListener("UpdateUserMsg",this::updateUserMsg);
    }

    public void removeListeners() {
        mf.getGameModel().removeListeners();
    }
}
