package client.view.main;

import client.model.IUserModel;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import misc.Status;

public class MainWindowViewModel {
    private final StringProperty username = new SimpleStringProperty();
    private final StringProperty password = new SimpleStringProperty();
    private final StringProperty loginResponse = new SimpleStringProperty();
    private final IUserModel userModel;

    public MainWindowViewModel(IUserModel userModel){
        this.userModel = userModel;
    }

    public boolean loginUser() {
        // handle the below
        // user not registered
        // wrong password
        // at success - change views to the menu
        String user = username.get();
        String pass = password.get();
        if (user == null || user.equals("")) {
            loginResponse.set("Username cannot be empty");
            return false;
        }

        if(pass == null || pass.equals("")) {
            loginResponse.set("Password cannot be empty");
            return false;
        }

        Status result = userModel.loginUser(user,pass);
        if(result.equals(Status.FAIL_USER_DOESNT_EXIST)){
            loginResponse.set("Username doesn't exist");
            return false;
        } else if (result.equals(Status.FAIL_ALREADY_LOGGED_IN)){
            loginResponse.set("User already logged in");
            return false;
        } else if(result.equals(Status.FAIL_WRONG_PASSWORD)){
            loginResponse.set("Wrong password");
            return false;
        }
        return true;        // STATUS.SUCCESS
    }

    public void clear() {
        username.set("");
        password.set("");
        loginResponse.set("");
    }

    public StringProperty loginResponseTextProperty() {
        return loginResponse;
    }

    public StringProperty usernameTextProperty() {
        return username;
    }

    public StringProperty passwordTextProperty() {
        return password;
    }
}
