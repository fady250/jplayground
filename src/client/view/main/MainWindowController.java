package client.view.main;

import client.core.ViewManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MainWindowController {

    @FXML
    public Button loginButton;
    @FXML
    public Button signupButton;
    @FXML
    public Label loginResponse;
    @FXML
    public TextField username;
    @FXML
    public TextField password;
    public Button connect4TopPlayers;
    public Button xoTopPlayers;

    private ViewManager vManager;
    private MainWindowViewModel mainWindowVM;


    public void init(ViewManager viewManager, MainWindowViewModel mainWindowVM) {
        this.vManager = viewManager;
        this.mainWindowVM = mainWindowVM;
        username.textProperty().bindBidirectional(mainWindowVM.usernameTextProperty());
        password.textProperty().bindBidirectional(mainWindowVM.passwordTextProperty());
        loginResponse.textProperty().bind(mainWindowVM.loginResponseTextProperty());
        mainWindowVM.clear();
    }

    public void LoginClicked(ActionEvent actionEvent) {
        if(mainWindowVM.loginUser()){
            vManager.setUserName(username.textProperty().get()); // set the username in the view manager in order to pass it to other views
            vManager.changeView("MenuView");
        }
    }

    public void SignupClicked(ActionEvent actionEvent) {
        vManager.openRegisterView();
    }


    public void xoTopClicked(ActionEvent actionEvent) {
        vManager.openScoreView("X-O");
    }

    public void connect4TopClicked(ActionEvent actionEvent) {
        vManager.openScoreView("Connect4");
    }
}
