package client.view.menu;

import client.core.ModelFactory;

public class GamesMenuViewModel {

    private final ModelFactory mf;

    public GamesMenuViewModel(ModelFactory mf) {
        this.mf = mf;
    }

    public void insertPlayerIntoGameQueue(String game) {
        mf.getGameModel().insertPlayerIntoGameQueue(game);
    }

    public void logoutUser(String username) {
        mf.getUserModel().logoutUser(username);
    }
}
