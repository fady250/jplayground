package client.view.menu;

import client.core.ViewManager;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class GamesMenuController {

    @FXML
    public Button XoButton;
    @FXML
    public Button Connect4Button;
    @FXML
    public Button logoutButton;
    public Label usernameLabel;


    private ViewManager vManager;
    private GamesMenuViewModel menueVM;

    public void init(ViewManager viewManager, GamesMenuViewModel menueVM ){
        this.vManager = viewManager;
        this.menueVM = menueVM;
        usernameLabel.setText("Welcome " + vManager.getUserName() + " ! ");
    }

    public void XoButtonClicked(ActionEvent actionEvent) {
        vManager.openXoView();
        menueVM.insertPlayerIntoGameQueue("xo");
    }

    public void Connect4ButtonClicked(ActionEvent actionEvent) {
        vManager.openConnect4View();
        menueVM.insertPlayerIntoGameQueue("connect4");
    }

    public void logoutButtonClicked(ActionEvent actionEvent) {
        menueVM.logoutUser(vManager.getUserName());
        vManager.changeView("MainWindow");
    }
}
