package client.view.score;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import misc.ScoreEntry;

public class ScoreWindowController {

    @FXML
    public TableColumn<String,String> ScoreColumn;
    public TableColumn<String,String> PlayerColumn;
    public TableView<ScoreEntry> tableView;

    public void init(ScoreWindowViewModel scoreVM, String game) {
        tableView.setItems(scoreVM.getScores(game));
        PlayerColumn.setCellValueFactory(new PropertyValueFactory<>("Player"));
        ScoreColumn.setCellValueFactory(new PropertyValueFactory<>("Score"));
    }
}
