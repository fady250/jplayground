package misc;

public class ScoreEntry {
    private final String Player;
    private final String Score;
    public ScoreEntry(String s1, String s2){
        Player = s1;
        Score = s2;
    }

    public String getPlayer(){
        return Player;
    }
    public String getScore(){
            return Score;
        }
}
