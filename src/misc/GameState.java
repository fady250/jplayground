package misc;

import java.io.Serializable;

public class GameState implements Serializable {
    private boolean playerWon;
    private boolean gameTie;

    public GameState(){
        playerWon = false;
        gameTie = false;
    }
    public void setAsWinner(){
        playerWon = true;
    }
    public boolean getWinningFlag(){return playerWon;}

    public boolean isGameTie() {
        return gameTie;
    }

    public void setGameTie() {
        this.gameTie = true;
    }
}
