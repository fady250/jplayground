package misc;

import java.io.Serializable;

public class Player implements Serializable {

    private IRMIClient player;
    public Player(IRMIClient p){
        player = p;
    }

    public IRMIClient getPlayer() {
        return player;
    }
}
