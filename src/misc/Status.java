package misc;

public enum Status {
    SUCCESS,
    FAIL_USER_EXIST,
    FAIL_USER_DOESNT_EXIST,
    FAIL_WRONG_PASSWORD,
    FAIL_ALREADY_LOGGED_IN
}
