package misc;

import java.io.Serializable;

public class GameStateConnect4 extends GameState implements Serializable {

    private final char[][] board;
    private int i,j;

    public GameStateConnect4(){
        board = new char[6][7];
        for (int i = 0; i < 6; i++)
            for (int j = 0; j < 7; j++)
                board[i][j] = ' ';
    }

    public char[][] getBoard() {
        return board;
    }

    public void setField(char c, int i, int j) {
        this.board[i][j] = c;
        this.i = i;
        this.j = j;
    }

    public int getI() {
        return i;
    }
    public int getJ() {
        return j;
    }
}
