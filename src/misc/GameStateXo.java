package misc;

import java.io.Serializable;

public class GameStateXo extends GameState implements Serializable {

    private final char[][] board;

    public GameStateXo(){
        board = new char[3][3];
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                board[i][j] = ' ';
    //    //players[Turn.PLAYER1.ordinal()] = c1;
    //    //players[Turn.PLAYER2.ordinal()] = c2;
    }

    public char[][] getBoard() {
        return board;
    }

    public void setField(char c, int i, int j) {
        this.board[i][j] = c;
    }
}
