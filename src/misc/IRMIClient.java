package misc;

import java.rmi.Remote;
import java.rmi.RemoteException;

/*
 * interface which represents the client
 * basically a stub for server to call methods on the client
 * needs to send reference to itself so server can reply it specifically
 */
public interface IRMIClient extends Remote {
    // methods that the server can run on client
    void updateID(int id) throws RemoteException;
    void setRole(char role) throws RemoteException;
    void notify(Notifications type) throws RemoteException;     // notify client about events
    void updateGui(GameState state) throws RemoteException;
    String getUsername() throws RemoteException;
}
