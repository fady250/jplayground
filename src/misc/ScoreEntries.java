package misc;

import java.io.Serializable;
import java.util.ArrayList;

public class ScoreEntries implements Serializable {

    ArrayList<String> list = null;

    public ScoreEntries() {
        list = new ArrayList<>();
    }

    public void addEntry(String s1, String s2) {
        list.add(s1);
        list.add(s2);
    }

   public ArrayList<String> getEntries() {
       return this.list;
   }
}
