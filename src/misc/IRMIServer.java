package misc;

import java.rmi.Remote;
import java.rmi.RemoteException;

/*
 * interface which represents the server
 * basically a stub for clients to call methods on the server
 */
public interface IRMIServer extends Remote {

    // methods that client wants to run on the server
    Status registerUser(String username, String password) throws RemoteException;
    Status loginUser(String username, String password, IRMIClient client) throws RemoteException;
    void logoutUser(String username) throws RemoteException;
    void insertPlayerIntoGameQueue(String game, IRMIClient client) throws RemoteException;
    void updateGameState(int id, GameState state) throws RemoteException;
    void gameWindowClosed(String game, int id) throws RemoteException;
    ScoreEntries getScores(String game) throws RemoteException;
}
