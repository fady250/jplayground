package server.networking;

import misc.*;
import server.database.DatabaseManagerImpl;
import server.model.ISessionManager;
import server.model.SessionManager;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RMIServerImpl implements IRMIServer {

    private ISessionManager sessionManager;         // this is the model

    public RMIServerImpl() throws RemoteException {
        UnicastRemoteObject.exportObject(this,0);           // 0 is the default port TODO verify port when not local host?
        sessionManager = new SessionManager(new DatabaseManagerImpl());     // init model
    }

    /**
     *
     * @throws RemoteException
     * @throws AlreadyBoundException
     */
    public void startServer() throws RemoteException, AlreadyBoundException {
        Registry registry = LocateRegistry.createRegistry(1099);          // default RMI port
        registry.bind("GameCenterServer",this);                     // for client  to be able to get the server by name
        System.out.println("Server up and running !");
    }


    // below methods that are exposed to clients

    @Override
    public Status registerUser(String username, String password) {
        return sessionManager.registerUser(username,password);
    }

    @Override
    public Status loginUser(String username, String password, IRMIClient client) {
        return sessionManager.loginUser(username,password,client);
    }

    @Override
    public void logoutUser(String username) throws RemoteException {
        sessionManager.logoutUser(username);
    }

    @Override
    public void insertPlayerIntoGameQueue(String game, IRMIClient client) {
        sessionManager.insertPlayerIntoGameQueue(game,client);
    }

    @Override
    public void updateGameState(int id, GameState state) throws RemoteException {
        sessionManager.updateGameState(id, state);
    }

    @Override
    public void gameWindowClosed(String game, int id) {
        sessionManager.gameWindowClosed(game, id);
    }

    @Override
    public ScoreEntries getScores(String game) throws RemoteException {
        return sessionManager.getScores(game);
    }

}
