package server.model;

import misc.GameState;
import misc.IRMIClient;
import misc.ScoreEntries;
import misc.Status;

public interface ISessionManager {
    Status registerUser(String username, String password);
    Status loginUser(String username, String password, IRMIClient client);
    void logoutUser(String username);
    void insertPlayerIntoGameQueue(String game, IRMIClient client);
    void updateGameState(int id, GameState state);
    void gameWindowClosed(String game, int id);
    ScoreEntries getScores(String game);
}
