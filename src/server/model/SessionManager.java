package server.model;

import misc.*;
import server.database.DatabaseManagerImpl;
import server.database.IDatabaseManager;

import java.rmi.RemoteException;
import java.util.*;

/**
 * Manages connected players
 * includes Queue for each game, whenever 2 players are pending they are added to the ongoing games table
 *
 */
public class SessionManager implements ISessionManager {

    private final IDatabaseManager DBManager;
    private final ArrayList<String> loggedInUsers;                // to prevent same user from logging in twice
    private final ArrayList<IRMIClient> PlayersQueueXo;           // Whenever queue has 2 players --> update DB and clear Queue
    private final ArrayList<IRMIClient> PlayersQueueConnect4;     // Whenever queue has 2 players --> update DB and clear Queue

    public SessionManager(DatabaseManagerImpl databaseManager) {
        this.loggedInUsers = new ArrayList<String>();
        this.PlayersQueueXo = new ArrayList<IRMIClient>();
        this.PlayersQueueConnect4 = new ArrayList<IRMIClient>();
        this.DBManager = databaseManager;
        this.DBManager.initializeDB();
    }


    @Override
    public Status registerUser(String username, String password) {
        if (DBManager.checkIfUserExist(username)) {
            return Status.FAIL_USER_EXIST;
        }
        DBManager.registerUser(username, password);
        return Status.SUCCESS;
    }

    @Override
    public Status loginUser(String username, String password, IRMIClient client) {
        if (!DBManager.checkIfUserExist(username)) {
            return Status.FAIL_USER_DOESNT_EXIST;
        } else if(loggedInUsers.contains(username)){        // prevent same user to login twice
            return Status.FAIL_ALREADY_LOGGED_IN;
        } else if (DBManager.queryPassword(username).equals(password)) {
            loggedInUsers.add(username);
        } else {
            return Status.FAIL_WRONG_PASSWORD;
        }
        return Status.SUCCESS;
    }

    @Override
    public void logoutUser(String username) {
        loggedInUsers.remove(username);
    }

    public void insertPlayerIntoGameQueue(String game, IRMIClient client) {
        ArrayList<IRMIClient> QueueRef = PlayersQueueXo;        // avoid null
        char r1 = ' ';
        char r2 = ' ';
        //if (game.equals("xo")) {
        //    PlayersQueueXo.add(client);
        //    if (PlayersQueueXo.size() == 2) {
        //        IRMIClient c1 = PlayersQueueXo.remove(1);
        //        IRMIClient c0 = PlayersQueueXo.remove(0);
        //        int id = DBManager.addActiveGame(game, new Player(c0), new Player(c1));
        //        // update ID in clients and grant turn to p0
        //        try {
        //            c0.updateID(id);
        //            c0.setXoRole('x');
        //            c1.updateID(id);
        //            c1.setXoRole('o');
        //            c0.notify(Notifications.YOUR_TURN);
        //        } catch (RemoteException e) {
        //            throw new RuntimeException("Could not contact Client");
        //        }
        //    }
        //}
        if (game.equals("xo")) {
            r1 = 'x';
            r2 = 'o';
        }else if (game.equals("connect4")) {
            QueueRef = PlayersQueueConnect4;
            r1 = 'r';
            r2 = 'y';
        }

        QueueRef.add(client);
        if (QueueRef.size() == 2) {
            IRMIClient c1 = QueueRef.remove(1);
            IRMIClient c0 = QueueRef.remove(0);
            int id = DBManager.addActiveGame(game, new Player(c0), new Player(c1));
            // update ID in clients and grant turn to p0
            try {
                c0.updateID(id);
                c0.setRole(r1);
                c1.updateID(id);
                c1.setRole(r2);
                c0.notify(Notifications.YOUR_TURN);
            } catch (RemoteException e) {
                throw new RuntimeException("Could not contact Client");
            }
        }
    }

    @Override
    public void updateGameState(int id, GameState state) {
        // first check if the player won , if yes - handle
        IRMIClient nextPlayer = DBManager.getNextPlayer(id).getPlayer();
        if(state.getWinningFlag()){
            try {
                nextPlayer.updateGui(state);
                nextPlayer.notify(Notifications.YOU_LOST);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            // add to score table -- move to private
            IRMIClient current = DBManager.getCurrentPlayer(id).getPlayer();
            String username;
            try {
                username = current.getUsername();
                System.out.println(username);
            }catch (RemoteException e) {
                throw new RuntimeException("Could not contact Client");
            }
            DBManager.updateScoreTables(id,username);
            // finally, remove entry from active games table
            DBManager.removeActiveGame(id);
        }else if(state.isGameTie()){
            try {
                nextPlayer.updateGui(state);
                nextPlayer.notify(Notifications.GAME_TIE);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            DBManager.removeActiveGame(id);
        }
        else { // need to grant turn to next player
            DBManager.updateTurn(id);
            try {
                nextPlayer.updateGui(state);
                nextPlayer.notify(Notifications.YOUR_TURN);
            } catch (RemoteException e) {
                throw new RuntimeException("Could not contact Client");
            }
        }
    }

    @Override
    public void gameWindowClosed(String game, int id) {
        if (game.equals("xo")){
            if(PlayersQueueXo.size() ==1 ){
                PlayersQueueXo.remove(0);
            }else{          // game already in progress
                handleOpponentQuit(id);
                //else - just return
            }
        }else if(game.equals("connect4")){
            if(PlayersQueueConnect4.size() == 1){
                PlayersQueueConnect4.remove(0);
            }else{          // game already in progress
                handleOpponentQuit(id);
                //else - just return
            }
        }
    }

    @Override
    public ScoreEntries getScores(String game) {
        return DBManager.getScoreTable(game);
   }

    private void handleOpponentQuit(int id) {
        Player player = DBManager.getNextPlayer(id);
        if(player != null){        // active game exists
            // notify both players that game ended
            IRMIClient nextPlayer = player.getPlayer();
            IRMIClient current = DBManager.getCurrentPlayer(id).getPlayer();
            try {
                nextPlayer.notify(Notifications.OPPONENT_QUIT);
                current.notify(Notifications.OPPONENT_QUIT);
            } catch (RemoteException e) {
                throw new RuntimeException("Could not contact Client");
            }
            // remove active game
            DBManager.removeActiveGame(id);
        }
    }
}