package server.database;

import misc.Player;
import misc.ScoreEntries;

import java.io.*;
import java.sql.*;

public class DatabaseManagerImpl implements IDatabaseManager {

    private PreparedStatement userExistPs;
    private PreparedStatement xoScoreUserExistPs;
    private PreparedStatement connect4ScoreUerExistPs;
    private PreparedStatement addUserPs;
    private PreparedStatement passwordQueryPs;
    private PreparedStatement addActiveGamePs;
    private PreparedStatement removeActiveGamePs;
    private PreparedStatement queryGameByIdPs;
    private PreparedStatement queryGameAndTurnPs;
    private PreparedStatement updateTurnPs;
    private PreparedStatement updateXoScorePs;
    private PreparedStatement updateConnect4ScorePs;
    private PreparedStatement queryXoScorePs;
    private PreparedStatement queryConnect4ScorePs;
    private PreparedStatement addXoScoreEntryPs;
    private PreparedStatement addConnect4ScoreEntryPs;
    private PreparedStatement queryXoScoreTablePs;
    private PreparedStatement queryConnect4ScoreTablePs;

    @Override
    public void initializeDB() {
        final String DATABASE_URL = "jdbc:derby:C:\\gamecenter\\src\\server\\database\\gamecenterdb";
        final String userExistQuery = "SELECT * FROM MAIN.USERS WHERE USERNAME = ?";
        final String xoScoreUserExistQuery = "SELECT * FROM MAIN.SCORE_XO WHERE USERNAME = ?";
        final String connect4ScoreUserExistQuery = "SELECT * FROM MAIN.SCORE_CONNECT4 WHERE USERNAME = ?";
        final String addUserQuery = "INSERT INTO MAIN.USERS (USERNAME, PASSWORD) VALUES (?,?)";
        final String passQuery = "SELECT PASSWORD FROM MAIN.USERS WHERE USERNAME = ?";
        final String addActiveGame = "INSERT INTO MAIN.ACTIVE_GAMES (GAME,PLAYER1,PLAYER2,TURN) VALUES (?,?,?,?)";
        final String removeActiveGame = "DELETE FROM MAIN.ACTIVE_GAMES WHERE ID = ?";
        final String queryGameById = "SELECT * FROM MAIN.ACTIVE_GAMES WHERE ID = ?";
        final String queryGameAndTurn = "SELECT GAME , TURN FROM MAIN.ACTIVE_GAMES WHERE ID = ?";
        final String updateTurn = "UPDATE MAIN.ACTIVE_GAMES SET TURN = ? WHERE ID = ?";
        final String updateXoScore = "UPDATE MAIN.SCORE_XO SET SCORE = ? WHERE USERNAME = ?";
        final String updateConnect4Score = "UPDATE MAIN.SCORE_CONNECT4 SET SCORE = ? WHERE USERNAME = ?";
        final String queryXoScore = "SELECT SCORE FROM MAIN.SCORE_XO WHERE USERNAME = ?";
        final String queryConnect4Score = "SELECT SCORE FROM MAIN.SCORE_CONNECT4 WHERE USERNAME = ?";
        final String addXoScoreEntry = "INSERT INTO MAIN.SCORE_XO (USERNAME,SCORE) VALUES (?,1)";
        final String addConnect4ScoreEntry = "INSERT INTO MAIN.SCORE_CONNECT4 (USERNAME,SCORE) VALUES (?,1)";
        final String queryXoScoreTable = "SELECT * FROM MAIN.SCORE_XO ORDER BY SCORE DESC";
        final String queryConnect4ScoreTable = "SELECT * FROM MAIN.SCORE_CONNECT4 ORDER BY SCORE DESC";

        try{
            Connection connection = DriverManager.getConnection(DATABASE_URL, "fady", "fady");
            Statement statement = connection.createStatement();
            userExistPs = connection.prepareStatement(userExistQuery);
            xoScoreUserExistPs = connection.prepareStatement(xoScoreUserExistQuery);
            connect4ScoreUerExistPs = connection.prepareStatement(connect4ScoreUserExistQuery);
            addUserPs = connection.prepareStatement(addUserQuery);
            passwordQueryPs = connection.prepareStatement(passQuery);
            addActiveGamePs = connection.prepareStatement(addActiveGame, statement.RETURN_GENERATED_KEYS);
            removeActiveGamePs = connection.prepareStatement(removeActiveGame);
            queryGameByIdPs = connection.prepareStatement(queryGameById);
            queryGameAndTurnPs = connection.prepareStatement(queryGameAndTurn);
            updateTurnPs = connection.prepareStatement(updateTurn);
            updateXoScorePs = connection.prepareStatement(updateXoScore);
            updateConnect4ScorePs = connection.prepareStatement(updateConnect4Score);
            queryXoScorePs = connection.prepareStatement(queryXoScore);
            queryConnect4ScorePs = connection.prepareStatement(queryConnect4Score);
            addXoScoreEntryPs = connection.prepareStatement(addXoScoreEntry);
            addConnect4ScoreEntryPs = connection.prepareStatement(addConnect4ScoreEntry);
            queryXoScoreTablePs = connection.prepareStatement(queryXoScoreTable);
            queryConnect4ScoreTablePs = connection.prepareStatement(queryConnect4ScoreTable);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean checkIfUserExist(String username) {
        ResultSet rs;
        try {
            userExistPs.setString(1,username);
            rs = userExistPs.executeQuery();
            return rs.next();
        } catch (SQLException e) {
            throw new RuntimeException("Could not access DataBase");
        }
    }

    @Override
    public void registerUser(String username, String password) {
        try {
            addUserPs.setString(1,username);
            addUserPs.setString(2,password);
            addUserPs.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Could not access DataBase");
        }
    }

    @Override
    public String queryPassword(String username) {
        ResultSet rs;
        try {
            passwordQueryPs.setString(1,username);
             rs = passwordQueryPs.executeQuery();
             if(rs.next())
                return rs.getString(1);
             else
                 return " ";
        } catch (SQLException e) {
            throw new RuntimeException("Could not access DataBase");
        }
    }

    @Override
    public int addActiveGame(String game, Player p1, Player p2) {
        byte[] bArray;
        int id = 0;
        try{
            addActiveGamePs.setString(1, game);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(p1);
            bArray = baos.toByteArray();
            addActiveGamePs.setBytes(2,bArray);

            baos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baos);
            oos.writeObject(p2);
            bArray = baos.toByteArray();
            addActiveGamePs.setBytes(3,bArray);
            addActiveGamePs.setString(4,"P1");
            oos.close();
            baos.close();
            // execute
            addActiveGamePs.executeUpdate();
            // get key
            ResultSet rs = addActiveGamePs.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            rs.close();
            return id;
        }catch (IOException | SQLException e) {
            throw new RuntimeException("Could not access DataBase");
        }
    }

    @Override
    public void removeActiveGame(int id) {
        try{
            removeActiveGamePs.setInt(1,id);
            removeActiveGamePs.executeUpdate();
        }catch (SQLException e) {
            throw new RuntimeException("Could not access DataBase");
        }
    }

    @Override
    public void updateTurn(int id) {
        try{
            queryGameAndTurnPs.setInt(1,id);
            ResultSet rs = queryGameAndTurnPs.executeQuery();
            if(rs.next()){
                String turn = rs.getString(2);
                if(turn.equals("P1")){
                    updateTurnPs.setString(1,"P2");
                }else {
                    updateTurnPs.setString(1,"P1");
                }
                updateTurnPs.setInt(2,id);
                updateTurnPs.executeUpdate();
            }
            rs.close();
        } catch (SQLException e) {
            throw new RuntimeException("Could not access DataBase");
        }
    }

    @Override
    public Player getNextPlayer(int id) {
        Player nextPlayer = null;
        byte[] st;
        Blob b;
        try {
            queryGameByIdPs.setInt(1,id);
            ResultSet rs = queryGameByIdPs.executeQuery();
            if (rs.next()) {
                String turn = rs.getString(5);
                if(turn.equals("P1")){
                    b = rs.getBlob(4);
                    st = b.getBytes(1,(int)b.length());
                }else{
                    b = rs.getBlob(3);
                    st = b.getBytes(1,(int)b.length());
                }
                ByteArrayInputStream baip = new ByteArrayInputStream(st);
                ObjectInputStream ois = new ObjectInputStream(baip);
                nextPlayer = (Player) ois.readObject();
                ois.close();
                baip.close();
            }
            rs.close();
        } catch (SQLException | IOException | ClassNotFoundException e) {
            throw new RuntimeException("Could not access DataBase");
        }
        return nextPlayer;
    }

    @Override
    public Player getCurrentPlayer(int id) {
        Player nextPlayer = null;
        byte[] st;
        Blob b;
        try {
            queryGameByIdPs.setInt(1,id);
            ResultSet rs = queryGameByIdPs.executeQuery();
            if (rs.next()) {
                String turn = rs.getString(5);
                if(turn.equals("P1")){
                    b = rs.getBlob(3);
                    st = b.getBytes(1,(int)b.length());
                }else{
                    b = rs.getBlob(4);
                    st = b.getBytes(1,(int)b.length());
                }
                ByteArrayInputStream baip = new ByteArrayInputStream(st);
                ObjectInputStream ois = new ObjectInputStream(baip);
                nextPlayer = (Player) ois.readObject();
                ois.close();
                baip.close();
            }
            rs.close();
        } catch (SQLException | IOException | ClassNotFoundException e) {
            throw new RuntimeException("Could not access DataBase");
        }
        return nextPlayer;
    }

    @Override
    public String getGameType(int id) {
        String game = null;
        try {
            queryGameAndTurnPs.setInt(1, id);
            ResultSet rs = queryGameAndTurnPs.executeQuery();
            if (rs.next()) {
                game =  rs.getString(1);
            }
            rs.close();
            return game;
        } catch (SQLException e) {
            throw new RuntimeException("Could not access DataBase");
        }
    }

    @Override
    public void updateScoreTables(int id,String username) {
        // check if there is entry with this user
        String game = this.getGameType(id);
        if(game.equals("xo")) {
            try {
                xoScoreUserExistPs.setString(1,username);
                ResultSet rs = xoScoreUserExistPs.executeQuery();
                if(rs.next()) {  // user exists
                    queryXoScorePs.setString(1, username);
                    rs = queryXoScorePs.executeQuery();
                    if (rs.next()) {
                        int score = rs.getInt(1);
                        score++;
                        updateXoScorePs.setInt(1,score);
                        updateXoScorePs.setString(2,username);
                        updateXoScorePs.executeUpdate();
                    }
                }else{      // insert new entry with score == 1
                    addXoScoreEntryPs.setString(1,username);
                    addXoScoreEntryPs.executeUpdate();
                }
            } catch (SQLException e) {
                throw new RuntimeException("Could not access DataBase");
            }
        }else if(game.equals("connect4")) {
            try {
                connect4ScoreUerExistPs.setString(1, username);
                ResultSet rs = connect4ScoreUerExistPs.executeQuery();
                if (rs.next()) {
                    queryConnect4ScorePs.setString(1, username);
                    rs = queryConnect4ScorePs.executeQuery();
                    if (rs.next()) {
                        int score = rs.getInt(1);
                        score++;
                        updateConnect4ScorePs.setInt(1, score);
                        updateConnect4ScorePs.setString(2, username);
                        updateConnect4ScorePs.executeUpdate();
                    }
                } else {      // insert new entry with score == 1
                    addConnect4ScoreEntryPs.setString(1, username);
                    addConnect4ScoreEntryPs.executeUpdate();
                }
            } catch (SQLException e) {
                throw new RuntimeException("Could not access DataBase");
            }
        }
    }

    @Override
    public ScoreEntries getScoreTable(String game) {
        ScoreEntries list = new ScoreEntries();
        try {
            ResultSet rs = queryXoScoreTablePs.executeQuery();
            if(game.equals("Connect4"))
                rs = queryConnect4ScoreTablePs.executeQuery();
            while(rs.next()){
                list.addEntry(rs.getString(1), rs.getString(2));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Could not access DataBase");
        }
        return list;
    }
}
