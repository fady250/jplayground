package server;

import server.networking.RMIServerImpl;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;

public class RunServer {
    public static void main(String[] args) throws RemoteException, AlreadyBoundException {

        RMIServerImpl server = new RMIServerImpl() ;            // instantiate the server
        server.startServer();                                   // puts the server object in the registry
    }
}
